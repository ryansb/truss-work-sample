#!/usr/bin/env python3

import re
import sys
import csv
import pytz
import traceback
from datetime import datetime, timedelta


def parse_duration(duration):
    hh, mm, ss, ms = re.match(r'(\d+):(\d+):(\d+)\.(\d+)$', duration).groups()
    total = timedelta(hours=int(hh), minutes=int(mm), seconds=int(ss), milliseconds=int(ms))
    return total.total_seconds()


def parse_timestamp(ts):
    pt_datetime = datetime.strptime(row['Timestamp'], '%m/%d/%y %I:%M:%S %p').replace(tzinfo=pytz.timezone('US/Pacific'))
    return pt_datetime.astimezone(pytz.timezone('US/Eastern')).isoformat()


def utf_warning_reader(lines):
    for line in lines:
        try:
            yield line.decode('utf-8').strip()
        except UnicodeDecodeError:
            sys.stderr.write('WARNING: %r has broken UTF-8\r\n' % line)
            yield line.decode('utf-8', errors='replace').strip()


if __name__ == '__main__':
    if sys.version_info.major != 3 or sys.version_info.minor < 5:
        print("Please use Python 3.5 or greater. Your version is: {}".format(sys.version))
        sys.exit(1)
    utf_sanitized = utf_warning_reader(sys.stdin.buffer.readlines())
    field_names = next(utf_sanitized).split(',')
    csv_out = csv.DictWriter(sys.stdout, fieldnames=field_names)
    csv_out.writeheader()
    for row in csv.DictReader(
            utf_sanitized,
            fieldnames=field_names,
            delimiter=','):
        try:
            row['ZIP'] = '00000'[0:5-len(row['ZIP'])] + row['ZIP']
            row['Timestamp'] = parse_timestamp(row['Timestamp'])
            row['FullName'] = row['FullName'].upper()
            row['BarDuration'] = parse_duration(row['BarDuration'])
            row['FooDuration'] = parse_duration(row['FooDuration'])
            row['TotalDuration'] = row['FooDuration'] + row['BarDuration']
        except:
            sys.stderr.write('Failed to parse line: %s\n' % row)
            #sys.stderr.write(traceback.format_exc())
            continue
        csv_out.writerow(row)
