# Truss Engineering Work Sample

## Installing Dependencies

This script requires Python 3.5 or greater and assumes Ubuntu 16.04 or later.

1. Ensure Python 3 and the `venv` utility are installed with `apt-get install python3 python3-venv`
2. Create a virtualenv with `python3 -m venv normalizer`
3. Switch to using the virtualenv python version and packages with `source normalizer/bin/activate`
3. Install dependencies (currently only pytz for timezone info) with `pip install -r requirements.txt`

## Running the Code

Now that `pytz` is installed, you can run `csv_normalizer.py`

```
# View the full output
$ python csv_normalizer.py <sample.csv

# View the warnings only
$ python csv_normalizer.py <sample.csv >/dev/null

# View the final CSV output only
$ python csv_normalizer.py <sample.csv 2>/dev/null
```
